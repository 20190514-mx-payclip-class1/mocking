package com.payclip.k21;

import java.util.List;

public class royaltyCalculator {

    private SalesRepository salesRepository;
    private CommissionCalculator commissionCalculator;

    public royaltyCalculator(SalesRepository salesRepository, CommissionCalculator commissionCalculator) {
        this.salesRepository = salesRepository;
        this.commissionCalculator = commissionCalculator;
    }

    public double calculateRoyalties(int year, int month) {

        double totalRoyalties = 0D;

        List<Integer> sales = salesRepository.getSales(year, month);

        for (Integer sale : sales) {
            totalRoyalties += calculateRoyalty(sale);
        }


        return totalRoyalties;
    }

    public double calculateRoyalty(Integer sale){
        double commission = CommissionCalculator.calculate(sale);
        double realSale = sale - commission;
        double royalty = realSale * 0.2D;

        return DoubleUtil.truncateToTwoDecimalPlaces(royalty);
    }

}
