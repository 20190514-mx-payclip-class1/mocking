package com.payclip.k21;

public class CommissionCalculator {

    private static final int LOWER_SALE_VALUE_THRESHOLD = 10000;
    private static final double LOWER_COMMISSION_PERCENTAGE = 0.05;
    private static final double HIGHER_COMMISSION_PERCENTAGE = 0.06;


    public static double calculate(double sale) {
        if (sale <= LOWER_SALE_VALUE_THRESHOLD) {
            return DoubleUtil.truncateToTwoDecimalPlaces(sale * LOWER_COMMISSION_PERCENTAGE);
        }
        return DoubleUtil.truncateToTwoDecimalPlaces(sale * HIGHER_COMMISSION_PERCENTAGE);
    }
}
