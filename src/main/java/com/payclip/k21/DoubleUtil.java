package com.payclip.k21;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class DoubleUtil {

    public static double truncateToTwoDecimalPlaces(double sale) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
        return Double.valueOf(df.format(sale));
    }
}
